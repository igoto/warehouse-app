package agoto.io.thewarehouse;

import org.junit.Test;

import java.util.List;

import agoto.io.thewarehouse.models.Product;
import agoto.io.thewarehouse.models.SearchResult;
import agoto.io.thewarehouse.utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * By GT.
 */
public class UtilsTest {

    @Test
    public void testDateFormatDateTime() throws Exception{
        String str_date = "2016-08-11T09:25:12";
        String actual = Utils.getFormattedDate(str_date);
        assertEquals("11 Aug 2016",actual);
    }

    @Test
    public void testDateFormatDate() throws Exception{
        String str_date = "2016-08-11";
        String actual = Utils.getFormattedDate(str_date);
        assertEquals("11 Aug 2016",actual);
    }

    @Test
    public void testDateFormatInvalid() throws Exception{
        String str_date = "201s6-08-1asdf1";
        String actual = Utils.getFormattedDate(str_date);
        assertEquals("",actual); // Should return empty result and should not throw exception
    }

    @Test
    public void testConvertSearchResultToProductListNull() throws Exception{
        List<Product> actual = Utils.convertSearchResultToProductList(null);
        assertNotNull(actual);
    }

    @Test
    public void testConvertSearchResultToProductList() throws Exception{
        SearchResult searchResult = TestUtils.getSearchResult1();
        List<Product> actual = Utils.convertSearchResultToProductList(searchResult);
        assertNotNull(actual);
        assertEquals(2, actual.size());
    }

    @Test
    public void testThumbToEnlargedNull() throws Exception{
        String enlargedUrl = Utils.thumbToEnlarged(null);
        assertNull(enlargedUrl);
    }

    @Test
    public void testThumbToEnlargedEmptyString() throws Exception{
        String enlargedUrl = Utils.thumbToEnlarged("");
        assertNotNull(enlargedUrl);
    }

    @Test
    public void testThumbToEnlargedEmpty() throws Exception{
        String url = "https://www.thewarehouse.co.nz/is-bin/intershop.static/WFS/TWL-B2C-Site/TWL-B2C/en_NZ/product/thumbnail/c4/9c/9400070043258_00_t.jpg";
        String expectedUrl = "https://www.thewarehouse.co.nz/is-bin/intershop.static/WFS/TWL-B2C-Site/TWL-B2C/en_NZ/product/enlarged/c4/9c/9400070043258_00_e.jpg";
        String actual = Utils.thumbToEnlarged(url);
        assertEquals(expectedUrl,actual);
    }
}
