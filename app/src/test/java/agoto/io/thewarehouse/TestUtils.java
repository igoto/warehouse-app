package agoto.io.thewarehouse;

import java.util.ArrayList;

import agoto.io.thewarehouse.models.Product;
import agoto.io.thewarehouse.models.SearchItem;
import agoto.io.thewarehouse.models.SearchResult;

/**
 * By GT.
 */
public class TestUtils {


    public static SearchResult getSearchResult1(){
        SearchResult searchResult = new SearchResult();
        searchResult.HitCount = 2L;
        searchResult.Results = new ArrayList<>();
        searchResult.Results.add(getSearchItem1());
        searchResult.Results.add(getSearchItem2());
        return searchResult;
    }

    public static SearchItem getSearchItem1(){
        SearchItem searchItem = new SearchItem();
        searchItem.Products = new ArrayList<>();
        searchItem.Products.add(getProduct1());
        return searchItem;
    }

    public static SearchItem getSearchItem2(){
        SearchItem searchItem = new SearchItem();
        searchItem.Products = new ArrayList<>();
        searchItem.Products.add(getProduct2());
        return searchItem;
    }

    public static Product getProduct1(){
        Product product = new Product();
        product.Barcode = "12345";
        product.Description = "Description1";
        return product;
    }

    public static Product getProduct2(){
        Product product = new Product();
        product.Barcode = "567890";
        product.Description = "Description2";
        return product;
    }

}
