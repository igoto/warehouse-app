package agoto.io.thewarehouse;

import android.support.test.InstrumentationRegistry;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

/**
 * By GT.
 */
public class MockServerDispatcher {


    public static Dispatcher getDispatcher(){
        Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                String path = request.getPath();
                if(path.contains("search.json")){
                    //SearchPage
                    return new MockResponse().setResponseCode(200).setBody(getSearchResponse());
                }else if(path.contains("newuser.json")){
                    //new User request
                    return new MockResponse().setResponseCode(200).setBody(getUserResponse());
                }else if(path.contains("price.json")){
                    //new User request
                    return new MockResponse().setResponseCode(200).setBody(getDetails());
                }
                return new MockResponse().setResponseCode(404);
            }
        };
        return dispatcher;
    }

    public static String getSearchResponse(){
        String fileName = "search_puma.json";
        String res = "";
        try {
            res = TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName);
        }catch (Exception e){
            //Do nothing
        }
        return res;
    }

    public static String getDetails(){
        String fileName = "product_details.json";
        String res = "";
        try {
            res = TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName);
        }catch (Exception e){
            //Do nothing
        }
        return res;
    }

    public static String getUserResponse(){
        String userFileName = "user_response.json";
        String userResponse = "";
        try {
            userResponse = TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), userFileName);
        }catch (Exception e){
            //Do nothing
        }
        return userResponse;
    }

}
