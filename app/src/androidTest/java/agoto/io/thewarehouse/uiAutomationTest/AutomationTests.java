package agoto.io.thewarehouse.uiAutomationTest;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.text.InputType;
import android.view.KeyEvent;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import agoto.io.thewarehouse.MockServerDispatcher;
import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.TestHelper;
import agoto.io.thewarehouse.activities.SearchActivity;
import agoto.io.thewarehouse.utils.WHConstants;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * By GT.
 *
 * UI automation test cases for testing user flows
 */
@RunWith(AndroidJUnit4.class)
public class AutomationTests {

    //Mockwebserver for mocking server responses
    private MockWebServer server;

    @Rule
    public ActivityTestRule<SearchActivity> mActivityRule = new ActivityTestRule<>(SearchActivity.class, true, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.setDispatcher(MockServerDispatcher.getDispatcher());
        server.start();
        WHConstants.WAREHOUSE_API_BASE_URL = server.url("/").toString();
    }

    @Test
    public void testBasicUI() throws Exception{
        mActivityRule.launchActivity(new Intent());
        onView(withText("Welcome")).check(matches(isDisplayed()));
        onView(withText("Scan Barcode")).check(matches(isDisplayed()));
        onView(withText("Search")).check(matches(isDisplayed()));
    }

    @Test
    public void testSearchFlow() throws Exception{
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.big_btn_search)).perform(click());
        onView(withHint("Search")).perform(typeText("test"), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withText("Puma Pioneer Backpack 1")).check(matches(isDisplayed()));
    }

    @Test
    public void testDetailsFlow() throws Exception{
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.big_btn_search)).perform(click());
        onView(withHint("Search")).perform(typeText("test"), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withText("Puma Pioneer Backpack 1")).perform(click());
        //Check details activity is opened
        onView(withId(R.id.tv_price)).check(matches(withText("19.99")));
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

}
