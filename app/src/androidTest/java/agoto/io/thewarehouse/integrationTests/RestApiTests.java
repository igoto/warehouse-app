package agoto.io.thewarehouse.integrationTests;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import agoto.io.thewarehouse.MockServerDispatcher;
import agoto.io.thewarehouse.models.SearchResult;
import agoto.io.thewarehouse.models.User;
import agoto.io.thewarehouse.services.ProductService;
import agoto.io.thewarehouse.services.UserService;
import agoto.io.thewarehouse.utils.WHConstants;
import okhttp3.mockwebserver.MockWebServer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * By GT.
 */
@RunWith(AndroidJUnit4.class)
public class RestApiTests {

    private User user = null;
    private SearchResult searchResult;
    private CountDownLatch lock = new CountDownLatch(1);
    private boolean error = false;

    @Before
    public void setUp() throws Exception {
        user = null;
        searchResult = null;
        lock = new CountDownLatch(1);
        error = false;
    }

    @Test
    public void testUserService() throws Exception{
        UserService
                .newUser()
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User u) {
                        user = u;
                        lock.countDown();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        error = true;
                        lock.countDown();
                    }
                });
        lock.await(2000, TimeUnit.MILLISECONDS);
        assertFalse(error);
        assertNotNull(user);
        assertTrue(StringUtils.isNotEmpty(user.UserID));
    }

    @Test
    public void testProductService() throws Exception{
        ProductService
                .searchProducts("Nike",0,10)
                .subscribe(new Action1<SearchResult>() {
                    @Override
                    public void call(SearchResult sr) {
                        searchResult = sr;
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        error = true;
                        lock.countDown();
                    }
                });
        lock.await(2000, TimeUnit.MILLISECONDS);
        assertFalse(error);
        assertNotNull(searchResult);
        assertNotNull(searchResult.Results);
    }


}
