package agoto.io.thewarehouse.instrumentedTests;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.KeyEvent;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import agoto.io.thewarehouse.MockServerDispatcher;
import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.activities.DetailsActivity;
import agoto.io.thewarehouse.activities.SearchActivity;
import agoto.io.thewarehouse.utils.WHConstants;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * By GT.
 */
@RunWith(AndroidJUnit4.class)
public class DetailsActivityTest {

    //Mockwebserver for mocking server responses
    private MockWebServer server;

    @Rule
    public ActivityTestRule<DetailsActivity> mActivityRule = new ActivityTestRule<>(DetailsActivity.class, true, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.setDispatcher(MockServerDispatcher.getDispatcher());
        server.start();
        WHConstants.WAREHOUSE_API_BASE_URL = server.url("/").toString();
    }

    @Test
    public void testDetailsActivity(){
        mActivityRule.launchActivity(new Intent());
        onView(withText("Barcode is invalid or not supplied")).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }
}
