package agoto.io.thewarehouse.activities;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.apache.commons.lang3.StringUtils;

import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.models.Detail;
import agoto.io.thewarehouse.models.Product;
import agoto.io.thewarehouse.services.ProductService;
import agoto.io.thewarehouse.utils.UserManager;
import agoto.io.thewarehouse.utils.Utils;
import agoto.io.thewarehouse.utils.WHConstants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DetailsActivity extends AppCompatActivity{

    @InjectView(R.id.product_image) SimpleDraweeView productImage;
    @InjectView(R.id.product_description) TextView productDescription;
    @InjectView(R.id.tv_price) TextView productPrice;
    @InjectView(R.id.tv_was_price) TextView productWasPrice;
    @InjectView(R.id.you_save) TextView youSave;
    @InjectView(R.id.price_info_text) TextView priceInfo;
    @InjectView(R.id.error_message) TextView errorMessage;
    @InjectView(R.id.barcode_scanned) TextView barcodeScanned;
    @InjectView(R.id.toolbar) Toolbar toolbar;
    @InjectView(R.id.scroll) View productContainer;

    @InjectView(R.id.error_container) View errorContainer;
    @InjectView(R.id.pb_main)CircularProgressBar progressBar;

    @InjectView(R.id.barcode) TextView tvBarcode;
    @InjectView(R.id.dept) TextView tvDept;
    @InjectView(R.id.sub_dept) TextView tvSubDept;

    @InjectView(R.id.tv_class) TextView tvClass;
    @InjectView(R.id.subClass) TextView tvSubClass;
    @InjectView(R.id.shim_loader) View shimLoader;


    private int colorGreen;
    private int colorUltraDark;

    private Detail detail;
    private Product product;
    private String barcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        colorGreen = ContextCompat.getColor(this,R.color.green);
        colorUltraDark = ContextCompat.getColor(this,R.color.ultra_dark);

        if(savedInstanceState != null && savedInstanceState.getSerializable(WHConstants.PRODUCT.DETAIL) != null){
            detail = (Detail) savedInstanceState.getSerializable(WHConstants.PRODUCT.DETAIL);
            if(detail == null || detail.Product == null){
                showError();
            }else {
                product = detail.Product;
                showProduct();
                initUI();
            }
        }else{
            if(!getIntent().hasExtra(WHConstants.PRODUCT.BARCODE)){
                //There is an error now .. Need to show the error message
                showError("Barcode is invalid or not supplied");
                return;
            }
            barcode = getIntent().getStringExtra(WHConstants.PRODUCT.BARCODE);
            barcodeScanned.setText(barcode);
            if(getIntent().hasExtra(WHConstants.PRODUCT.PRODUCT)){
                product = (Product) getIntent().getSerializableExtra(WHConstants.PRODUCT.PRODUCT);
                loadImage(false);
                showDescription();
                showPartialLoading();
            }else{
                showFullLoading();
            }
            fetchProduct();
        }
    }

    private void fetchProduct() {
        ProductService.fetchProduct(barcode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Detail>() {
                    @Override
                    public void call(Detail detail) {
                        if(detail == null || detail.Product == null){
                            showError(getString(R.string.error_barcode));
                        }else {
                            DetailsActivity.this.product = detail.Product;
                            showProduct();
                            initUI();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if(throwable != null && throwable instanceof java.net.UnknownHostException){
                            // Device is offline..
                            showError("Unable to reach Warehouse servers.\nPlease check your internet connection");
                            return;
                        }
                        if(throwable != null && StringUtils.isNotBlank(throwable.getMessage()) && throwable.getMessage().contains(WHConstants.SERICEERROR.INVALID_USER)){
                            // User stored in the sharedpreference is invalid. Delete the user.
                            UserManager.getSharedInstance().delUser();
                            showError("Error while searching products. Try again");
                            return;
                        }
                        showError(getString(R.string.error_barcode));
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(detail != null){
            outState.putSerializable(WHConstants.PRODUCT.DETAIL, detail);
        }
    }

    private void showError() {
        String msg = getString(R.string.error_while_loading_product);
        showError(msg);
    }

    private void showProduct(){
        errorContainer.setVisibility(View.GONE);
        productContainer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        shimLoader.setVisibility(View.GONE);
    }

    private void showError(String msg){
        errorMessage.setText(msg);
        errorContainer.setVisibility(View.VISIBLE);
        productContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        shimLoader.setVisibility(View.GONE);
    }

    private void showFullLoading(){
        errorContainer.setVisibility(View.GONE);
        productContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        shimLoader.setVisibility(View.GONE);
    }

    private void showPartialLoading(){
        errorContainer.setVisibility(View.GONE);
        productContainer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        shimLoader.setVisibility(View.VISIBLE);
    }


    private void initUI(){
        if(product == null) return;
        loadImage(true);
        showDescription();
        showPriceDetails();
        showOtherDetails();
    }

    private void loadImage(boolean enlarged) {
        if(StringUtils.isBlank(product.ImageURL)) return;
        String url = enlarged ? Utils.thumbToEnlarged(product.ImageURL) : product.ImageURL;
        try {
            Uri uri = Uri.parse(url);
            productImage.setImageURI(uri);
        } catch (Exception e) {
            //Do nothing .. Don't load the image.. URL is invalid from the backend service
        }
    }

    private void showDescription(){
        if(StringUtils.isBlank(product.Description)) return;
        setTitle(product.Description);
        productDescription.setText(product.Description);
    }

    private void showPriceDetails(){
        if(product.Price == null) return;
        showPrice();
        showWasPriced();
        showYouSave();
        showPriceInfoText();
    }


    private void showPrice(){
        if(StringUtils.isBlank(product.Price.Price)) return;
        productPrice.setText(product.Price.Price);
    }

    private void showWasPriced(){
        if(StringUtils.isBlank(product.Price.WasPrice)) return;
        String wasPriced = product.Price.WasPrice;
        SpannableString txt = new SpannableString(wasPriced);
        txt.setSpan(new StrikethroughSpan(),0,wasPriced.length(),0);
        productWasPrice.setText(txt);
    }

    private void showYouSave(){
        if(StringUtils.isNotBlank(product.Price.Savings)){
            SpannableStringBuilder builder = new SpannableStringBuilder();
            builder.append("You save ");
            int start = builder.length();
            builder.append("$ ").append(product.Price.Savings);
            builder.setSpan(new ForegroundColorSpan(colorGreen),start,builder.length(),0);
            builder.setSpan(new StyleSpan(Typeface.BOLD),start,builder.length(),0);
            youSave.setText(builder);
        }else if(StringUtils.isNotBlank(product.Price.Discount)){
            SpannableStringBuilder builder = new SpannableStringBuilder();
            builder.append("Total discount ");
            int start = builder.length();
            builder.append(product.Price.Discount).append(" %");
            builder.setSpan(new ForegroundColorSpan(colorGreen),start,builder.length(),0);
            builder.setSpan(new StyleSpan(Typeface.BOLD),start,builder.length(),0);
            youSave.setText(builder);
        }
    }

    private void showPriceInfoText() {
        if(StringUtils.isBlank(product.Price.ValidUntil)){
            priceInfo.setText(getString(R.string.best_price));
            return;
        }

        String validTill = Utils.getFormattedDate(product.Price.ValidUntil);
        if(StringUtils.isBlank(validTill)) return;

        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append("Discount valid until ");
        int start = builder.length();
        builder.append(validTill);
        builder.setSpan(new ForegroundColorSpan(colorUltraDark),start,builder.length(),0);
        builder.setSpan(new StyleSpan(Typeface.BOLD),start,builder.length(),0);
        priceInfo.setText(builder);
    }


    private void showOtherDetails(){
        if(StringUtils.isNotBlank(product.Barcode)){
            tvBarcode.setText(product.Barcode);
        }

        if(StringUtils.isNotBlank(product.Dept)){
            tvDept.setText(product.Dept);
        }
        if(StringUtils.isNotBlank(product.SubDept)){
            tvSubDept.setText(product.SubDept);
        }

        if(StringUtils.isNotBlank(product.Class0)){
            tvClass.setText(product.Class0);
        }

        if(StringUtils.isNotBlank(product.SubClass)){
            tvSubClass.setText(product.SubClass);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
