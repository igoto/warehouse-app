package agoto.io.thewarehouse.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.adapters.ProductListAdapter;
import agoto.io.thewarehouse.models.Product;
import agoto.io.thewarehouse.models.SearchResult;
import agoto.io.thewarehouse.services.ProductService;
import agoto.io.thewarehouse.utils.UserManager;
import agoto.io.thewarehouse.utils.Utils;
import agoto.io.thewarehouse.utils.WHConstants;
import br.com.mauker.materialsearchview.MaterialSearchView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import me.sudar.zxingorient.ZxingOrient;
import me.sudar.zxingorient.ZxingOrientResult;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();
    private static final int REQUEST_CAMERA = 0x00000011;
    private static final int LIST_FETCH_THRESHOLD = 20;
    private static final int LIMIT = 200;

    @InjectView(R.id.welcome_container) View mWelcomeContainer;

    @InjectView(R.id.search_view) MaterialSearchView searchView;
    @InjectView(R.id.coordinator_layout) CoordinatorLayout mCoordinatorLayout;

    @InjectView(R.id.rv_products) RecyclerView mRecyclerView;
    @InjectView(R.id.pb_main) CircularProgressBar progressBar;
    @InjectView(R.id.error_message) TextView mErrorMessage;
    @InjectView(R.id.shim) View mShim;

    @InjectView(R.id.action_floating_action_menu)FloatingActionsMenu mFloatingActionMenu;
    @InjectView(R.id.action_scan)FloatingActionButton mScanBarcode;
    @InjectView(R.id.action_search) FloatingActionButton mSearchProduct;

    @InjectView(R.id.big_btn_search) View mBigBtnSearchProduct;
    @InjectView(R.id.big_btn_scan) View mBigBtnScanProduct;

    private ProductListAdapter productListAdapter;
    private LinearLayoutManager mLayoutManager;

    private boolean isLoading = true;
    private int startAt = 0;

    private String searchQuery;

    private ActionBar ab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ab = getSupportActionBar();
        if(ab != null){
            ab.setDisplayHomeAsUpEnabled(false);
        }
        mFloatingActionMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                mShim.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                mShim.setVisibility(View.GONE);
            }
        });


        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewOpened() {
                // Do something once the view is open.
            }

            @Override
            public void onSearchViewClosed() {
                // Do something once the view is closed.
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Do something when the suggestion list is clicked.
                String suggestion = searchView.getSuggestionAtPosition(position);
//                searchView.setQuery(suggestion, false);
                searchView.closeSearch();
                doSearch(suggestion);
            }
        });
        searchView.adjustTintAlpha(0.95f);
        productListAdapter = new ProductListAdapter(onRowClickListener);
        mRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(productListAdapter);

        mBigBtnScanProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startBarcodeScanner();
            }
        });

        mBigBtnSearchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.openSearch();
            }
        });

        mScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingActionMenu.collapse();
                startBarcodeScanner();
            }
        });

        mSearchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingActionMenu.collapse();
                searchView.openSearch();
            }
        });
    }


    private void doSearch(String q){
        if(StringUtils.isBlank(q)) return;
        setTitle(q);
        ab.setDisplayHomeAsUpEnabled(true);
        mWelcomeContainer.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        searchQuery = q;
        isLoading = true;
        productListAdapter.setProducts(new ArrayList<Product>());
        showLoading();
        startAt = 0;
        searchProduct();
        mRecyclerView.addOnScrollListener(rvScrollListerner);
    }

    private void searchProduct() {
        ProductService
                .searchProducts(searchQuery, startAt, LIMIT)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SearchResult>() {
                    @Override
                    public void call(SearchResult searchResult) {
                        hideLoading();
                        if(searchResult == null || searchResult.Results == null || searchResult.Results.size() == 0){
                            if(startAt == 0) showError();
                            mRecyclerView.removeOnScrollListener(rvScrollListerner);
                            return;
                        }
                        if(searchResult.Results.size() < LIMIT-1) {
                            mRecyclerView.removeOnScrollListener(rvScrollListerner);
                        }

                        if(startAt == 0){
                            Snackbar.make(mRecyclerView, Long.toString(searchResult.HitCount) + " products found", Snackbar.LENGTH_LONG).show();
                        }

                        List<Product> products = Utils.convertSearchResultToProductList(searchResult);
                        productListAdapter.addProducts(products);
                        startAt = startAt + LIMIT;
                        isLoading = false;
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if(startAt == 0){
                            if(throwable != null && throwable instanceof java.net.UnknownHostException){
                                // Device is offline..
                                showError("Unable to reach Warehouse servers.\nPlease check your internet connection");
                                return;
                            }
                            if(throwable != null && StringUtils.isNotBlank(throwable.getMessage()) && throwable.getMessage().contains(WHConstants.SERICEERROR.INVALID_USER)){
                                // User stored in the sharedpreference is invalid. Delete the user.
                                UserManager.getSharedInstance().delUser();
                            }
                            showError("Error while searching products. Try again");
                            searchQuery = null;
                        }else{
                            mRecyclerView.removeOnScrollListener(rvScrollListerner);
                            Snackbar.make(mRecyclerView, "Error while fetching more products.", Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private View.OnClickListener onRowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Product p = (Product)view.getTag(R.id.fl_product_row);
            Intent intent = new Intent(SearchActivity.this, DetailsActivity.class);
            intent.putExtra(WHConstants.PRODUCT.PRODUCT, p);
            intent.putExtra(WHConstants.PRODUCT.BARCODE, p.Barcode);
            startActivity(intent);
        }
    };

    RecyclerView.OnScrollListener rvScrollListerner = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (isLoading) return; // Return if the page is currently being loading
            int totalItemCount = mLayoutManager.getItemCount();
            int lastPos = mLayoutManager.findLastCompletelyVisibleItemPosition();
            if (totalItemCount - lastPos < LIST_FETCH_THRESHOLD) {
                fetchMore();
            }
        }
    };

    private void fetchMore(){
        isLoading = true;
        searchProduct();
    }


    private void showLoading(){
        mErrorMessage.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(){
        String errorMessage = getString(R.string.no_products_found);
        showError(errorMessage);
    }

    private void showError(String msg){
        mErrorMessage.setText(msg);
        mErrorMessage.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void hideError(){
        mErrorMessage.setVisibility(View.GONE);
    }

    private void showWelcome(){
        hideLoading();
        hideError();
        ab.setDisplayHomeAsUpEnabled(false);
        setTitle(R.string.app_name);
        mRecyclerView.setVisibility(View.GONE);
        mWelcomeContainer.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle toolbar item clicks here. It'll
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_search){
            searchView.openSearch();
            return true;
        }

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        searchView.clearSuggestions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchView.activityResumed();
    }

    @Override
    public void onBackPressed() {
        if (searchView.isOpen()) {
            // Close the search on the back button press.
            searchView.closeSearch();
        } else if(mFloatingActionMenu.isExpanded()){
            mFloatingActionMenu.collapse();
        } else if(mWelcomeContainer.getVisibility() != View.VISIBLE){
            showWelcome();
        }else {
            super.onBackPressed();
        }
    }


    private void startBarcodeScanner(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }
        new ZxingOrient(this).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        ZxingOrientResult scanResult =  ZxingOrient.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle the result
            String barcodeVal = scanResult.getContents();
            if(StringUtils.isBlank(barcodeVal) || !StringUtils.isNumeric(barcodeVal)){
                Snackbar.make(mFloatingActionMenu, "Invalid Barcode Value", Snackbar.LENGTH_LONG).show();
            }else {
                Intent i = new Intent(SearchActivity.this, DetailsActivity.class);
                i.putExtra(WHConstants.PRODUCT.BARCODE, barcodeVal);
                startActivity(i);
            }
        }
    }


    private void requestCameraPermission() {
        Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            Log.i(TAG, "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(mCoordinatorLayout, R.string.permission_camera_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(SearchActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},  REQUEST_CAMERA);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new ZxingOrient(this).initiateScan();
                } else {
                    Snackbar.make(mCoordinatorLayout, R.string.camera_denied, Snackbar.LENGTH_LONG).show();
                }
            }
            break;
        }
    }


}
