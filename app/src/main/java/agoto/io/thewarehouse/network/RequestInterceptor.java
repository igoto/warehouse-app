package agoto.io.thewarehouse.network;

import java.io.IOException;

import agoto.io.thewarehouse.utils.WHConstants;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * By GT.
 */
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request()
                        .newBuilder()
                        .addHeader(WHConstants.WAREHOUSE_SUBSCRIPTION_KEY_NAME, WHConstants.WAREHOUSE_SUBSCRIPTION_KEY).build();
        return chain.proceed(request);
    }

}
