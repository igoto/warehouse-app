package agoto.io.thewarehouse.network;

import agoto.io.thewarehouse.models.Detail;
import agoto.io.thewarehouse.models.SearchResult;
import agoto.io.thewarehouse.models.User;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * By GT.
 */
public interface WarehouseAPI {

    @GET("search.json")
    Observable<SearchResult> search(@Query("UserID") String userID, @Query("Search") String search, @Query("Start") int startAt, @Query("Limit") int limit, @Query("Branch") int branch );

    @GET("newuser.json")
    Observable<User> newUser();

    @GET("price.json")
    Observable<Detail> details(@Query("UserID") String userID, @Query("Barcode") String barcode, @Query("Branch") int branch, @Query("MachineID") int machineId);

}
