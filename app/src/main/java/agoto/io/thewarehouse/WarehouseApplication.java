package agoto.io.thewarehouse;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.concurrent.TimeUnit;

import agoto.io.thewarehouse.network.RequestInterceptor;
import agoto.io.thewarehouse.utils.UserManager;
import agoto.io.thewarehouse.utils.WHConstants;
import okhttp3.OkHttpClient;

/**
 * By GT.
 */
public class WarehouseApplication extends Application {

    private static final String TAG = "WHAPP";

    private static WarehouseApplication currentInstance;

    private OkHttpClient okClient;

    @Override
    public void onCreate() {
        super.onCreate();
        currentInstance = this;
        Fresco.initialize(this);
        UserManager.getSharedInstance().initialize(); // Initialize User Manager
    }

    public static WarehouseApplication getCurrentInstance(){
        return currentInstance;
    }

    public OkHttpClient getCustomOKClient(){
        if(okClient == null){
            okClient = new OkHttpClient
                    .Builder()
                    .addInterceptor(new RequestInterceptor())
                    .connectTimeout(WHConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(WHConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .build();
        }
        return okClient;
    }

    public Point getScreenDimensions(){
        Point size = new Point();
        ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(size);
        return size;
    }



}
