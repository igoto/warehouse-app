package agoto.io.thewarehouse.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.adapters.viewHolders.ProductViewHolder;
import agoto.io.thewarehouse.models.Product;

/**
 * By GT.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private List<Product> products = new ArrayList<>();
    private View.OnClickListener rowClickListener;

    public ProductListAdapter(View.OnClickListener rowClickListener) {
        this.rowClickListener = rowClickListener;
    }

    public void addProducts(List<Product> p){
        products.addAll(p);
        notifyDataSetChanged();
    }

    public void setProducts(List<Product> p){
        products = p;
        notifyDataSetChanged();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product, parent, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder vh, int position) {
        vh.bind(products.get(position),rowClickListener);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
