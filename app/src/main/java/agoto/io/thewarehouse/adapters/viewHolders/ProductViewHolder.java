package agoto.io.thewarehouse.adapters.viewHolders;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.apache.commons.lang3.StringUtils;

import agoto.io.thewarehouse.R;
import agoto.io.thewarehouse.models.Product;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * By GT.
 */
public class ProductViewHolder extends RecyclerView.ViewHolder{

    @InjectView(R.id.product_image) public SimpleDraweeView productImage;
    @InjectView(R.id.tv_product_description) public TextView productDescription;
    @InjectView(R.id.tv_product_class) public TextView productClass;
    @InjectView(R.id.tv_product_sub_class) public TextView productSubClass;

    @InjectView(R.id.fl_product_row) public FrameLayout mProductShim;

    public ProductViewHolder(View v) {
        super(v);
        ButterKnife.inject(this, v);
    }

    public void bind(Product p, View.OnClickListener rowClickListener){
        if(p == null) return;
        mProductShim.setTag(R.id.fl_product_row,p);
        mProductShim.setOnClickListener(rowClickListener);
        loadImage(p);
        showDescription(p);
        showClass(p);
        showSubClass(p);
    }

    private void showSubClass(Product p) {
        if(StringUtils.isBlank(p.SubClass)) return;
        productSubClass.setText(p.SubClass);
    }

    private void showClass(Product p) {
        if(StringUtils.isBlank(p.Class0)) return;
        productClass.setText(p.Class0);
    }

    private void loadImage(Product p) {
        if(StringUtils.isBlank(p.ImageURL)) return;
        Uri uri = Uri.parse(p.ImageURL);
        productImage.setImageURI(uri);
    }

    private void showDescription(Product p){
        if(StringUtils.isBlank(p.Description)) return;
        productDescription.setText(p.Description);
    }

}
