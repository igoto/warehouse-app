package agoto.io.thewarehouse.services;

import agoto.io.thewarehouse.WarehouseApplication;
import agoto.io.thewarehouse.network.WarehouseAPI;
import agoto.io.thewarehouse.utils.WHConstants;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * By GT.
 */
public class AbstractBaseService {

    private static WarehouseAPI client;

    protected static WarehouseAPI WHAPI(){
        if(client == null){
            client = createClient();
        }
        return client;
    }

    private static WarehouseAPI createClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WHConstants.WAREHOUSE_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(WarehouseApplication.getCurrentInstance().getCustomOKClient())
                .build();

        return retrofit.create(WarehouseAPI.class);
    }
}
