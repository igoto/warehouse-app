package agoto.io.thewarehouse.services;

import org.apache.commons.lang3.StringUtils;

import agoto.io.thewarehouse.models.Detail;
import agoto.io.thewarehouse.models.SearchResult;
import agoto.io.thewarehouse.models.User;
import agoto.io.thewarehouse.utils.UserManager;
import agoto.io.thewarehouse.utils.WHConstants;
import rx.Observable;
import rx.functions.Func1;

/**
 * By GT.
 */
public class ProductService extends AbstractBaseService{

    public static Observable<SearchResult> searchProducts(final String search, final int startAt, final  int limit){
        return UserManager
                .getSharedInstance()
                .getUserForServiceCall()
                .flatMap(new Func1<User, Observable<SearchResult>>() {
                    @Override
                    public Observable<SearchResult> call(User user) {
                        String uid = (user == null || StringUtils.isBlank(user.UserID)) ? "" : user.UserID;//Null safe
                        return WHAPI().search(uid, search, startAt, limit, WHConstants.WAREHOUSE_BRANCH);
                    }
                });
    }

    public static Observable<Detail> fetchProduct(final String barcode){
        return UserManager
                .getSharedInstance()
                .getUserForServiceCall()
                .flatMap(new Func1<User, Observable<Detail>>() {
                    @Override
                    public Observable<Detail> call(User user) {
                        String uid = (user == null || StringUtils.isBlank(user.UserID)) ? "" : user.UserID;//Null safe
                        return WHAPI().details(uid, barcode, WHConstants.WAREHOUSE_BRANCH,WHConstants.WAREHOUSE_MACHINEID);
                    }
                });
    }



}
