package agoto.io.thewarehouse.services;

import agoto.io.thewarehouse.models.User;
import rx.Observable;

/**
 * By GT.
 */
public class UserService extends AbstractBaseService {

    public static Observable<User> newUser(){
        return WHAPI().newUser();
    }
}
