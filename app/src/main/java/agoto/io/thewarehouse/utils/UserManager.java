package agoto.io.thewarehouse.utils;

import android.content.SharedPreferences;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import agoto.io.thewarehouse.WarehouseApplication;
import agoto.io.thewarehouse.models.User;
import agoto.io.thewarehouse.services.UserService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * By GT.
 */
public class UserManager {

    private static final String TAG = UserManager.class.getSimpleName();
    private static final String PREFS_NAME = "agoto.io.thewarehouse.sharedpreferences";
    private static final String USERID = "UserID";

    private static volatile UserManager sharedInstance;
    private volatile User user;

    // Private constructor
    private UserManager(){
        this.user = getUserFromSharedPreference();
    }

    public boolean isUserEmpty(){
        return user == null || StringUtils.isBlank(user.UserID);
    }

    public User getUser(){
        if(user == null || StringUtils.isBlank(user.UserID)){
            synchronized (this){
                if(user == null || StringUtils.isBlank(user.UserID)){
                    user = getUserFromSharedPreference();
                }
            }
        }
        return user;
    }

    public static UserManager getSharedInstance(){
        if(sharedInstance == null){
            synchronized (UserManager.class){
                if(sharedInstance == null){
                    sharedInstance = new UserManager();
                }
            }
        }
        return sharedInstance;
    }

    public synchronized User getUserFromSharedPreference(){
        SharedPreferences sp = WarehouseApplication.getCurrentInstance().getSharedPreferences(PREFS_NAME,0);
        String userId = sp.getString(USERID, null);
        if(StringUtils.isBlank(userId)) return null;
        user = new User();
        user.UserID = userId;
        return user;
    }

    public synchronized void saveUser(User u){
        if(u == null || StringUtils.isBlank(u.UserID)) return;
        this.user = u;
        SharedPreferences sp = WarehouseApplication.getCurrentInstance().getSharedPreferences(PREFS_NAME,0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(USERID, u.UserID);
        editor.apply();
    }

    public synchronized void delUser(){
        this.user = null;
        SharedPreferences sp = WarehouseApplication.getCurrentInstance().getSharedPreferences(PREFS_NAME,0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(USERID, null);
        editor.apply();
    }

    public void initialize(){
        if(isUserEmpty()){
            UserService
                    .newUser()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<User>() {
                        @Override
                        public void call(User u) {
                            UserManager.getSharedInstance().saveUser(u);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            //Do nothing. Just log the error
                            if(throwable != null){
                                Log.e(TAG, throwable.getMessage());
                            }
                        }
                    });
        }
    }

    public Observable<User> getUserForServiceCall(){
        if(!isUserEmpty()){
            return Observable.just(user);
        }
        return UserService.newUser().doOnNext(new Action1<User>() {
            @Override
            public void call(User u) {
                saveUser(u);
            }
        });
    }

}
