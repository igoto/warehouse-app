package agoto.io.thewarehouse.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import agoto.io.thewarehouse.WarehouseApplication;
import agoto.io.thewarehouse.models.Product;
import agoto.io.thewarehouse.models.SearchItem;
import agoto.io.thewarehouse.models.SearchResult;

/**
 * By GT.
 */
public class Utils {

    public static List<Product> convertSearchResultToProductList(SearchResult searchResult){
        List<Product> temp = new ArrayList<>();
        if(searchResult == null || searchResult.Results == null || searchResult.Results.size() == 0) return temp;
        for (SearchItem si : searchResult.Results) {
            List<Product> p = si.Products;
            if(p != null && p.size()!=0) temp.addAll(p);
        }
        return temp;
    }

    public static String thumbToEnlarged(String thumbUrl){
        //enlarged
        if(StringUtils.isBlank(thumbUrl)) return thumbUrl;
        return thumbUrl.replace("thumbnail","enlarged").replace("_t.jpg","_e.jpg");
    }

    public static String getFormattedDate(String str_date){
        String myDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(str_date);
            myDate = new SimpleDateFormat("dd MMM yyyy").format(date);
        } catch (ParseException e) {
            //Do nothing not able to parse date. Backend problem
        }
        if(StringUtils.isBlank(myDate)){
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(str_date);
                myDate = new SimpleDateFormat("dd MMM yyyy").format(date);
            } catch (ParseException e) {
                //Do nothing not able to parse date. Backend problem
            }
        }
        return myDate;
    }

    //Not using this now..
    public static boolean isNetworkConnected(){
        ConnectivityManager cm = (ConnectivityManager) WarehouseApplication.getCurrentInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&  activeNetwork.isConnectedOrConnecting();
    }


}
