package agoto.io.thewarehouse.utils;

/**
 * By GT.
 */
public class WHConstants {

    //This should not be final. It should be based on different environment. Testing, production etc.
    // And should be loaded from configuration files.
    public static String WAREHOUSE_API_BASE_URL = "https://twg.azure-api.net/bolt/";

    public static final String WAREHOUSE_SUBSCRIPTION_KEY = "910d8bc132ca4f4b9a9f87ec87089978";
    public static final String WAREHOUSE_SUBSCRIPTION_KEY_NAME = "Ocp-Apim-Subscription-Key";
    public static final int WAREHOUSE_BRANCH = 208; // Hardcoded Branch
    public static final int WAREHOUSE_MACHINEID = 208; // Hardcoded Machineid
    public static final long CONNECTION_TIMEOUT = 10L; //10 seconds connection timeout

    public interface PRODUCT{
        String PRODUCT = "product";
        String DETAIL = "detail";
        String PRODUCTKEY = "ProductKey";
        String BARCODE = "barcode";
    }

    public interface PRICE{
        String ADV = "adv"; // – product discount,
        String GRP = "grp"; // – product group (hierarchy) discount,
        String STO = "sto"; // – manager’s special or manager’s clearance
        String CLR = "clr"; // – clearance
    }

    public interface SERICEERROR{
        String INVALID_USER = "Invalid UserID";

    }

}
