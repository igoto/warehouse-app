package agoto.io.thewarehouse.models;

import java.io.Serializable;

/**
 * By GT.
 */
public class Product implements Serializable {

    public String Class0;
    public String Barcode;
    public String ItemDescription;
    public String DeptID;
    public String SubClass;
    public String Class0ID;
    public String SubDeptID;
    public String Description;
    public String ItemCode;
    public String SubDept;
    public String ClassID;
    public String ImageURL;
    public String Dept;
    public String SubClassID;
    public String Class;
    public Long ProductKey;
    public Price Price;

}
