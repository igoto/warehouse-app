package agoto.io.thewarehouse.models;

import java.io.Serializable;

/**
 * By GT.
 */
public class Detail implements Serializable {

    public Product Product;
    public Integer MachineID; // 208
    public String Action; // S
    public String ScanBarcode; // 9400070043272
    public Integer ScanID; // 33349
    public String UserDescription;
    public String ProdQAT; // Prod
    public String ScanDateTime; // 2016-08-11T09:25:12
    public String Found; // Y
    public String UserID; // CEC81748-AEA0-47F4-8D45-F914B06DC7CA
    public Integer Branch;
}
