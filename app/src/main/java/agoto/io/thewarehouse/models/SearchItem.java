package agoto.io.thewarehouse.models;

import java.io.Serializable;
import java.util.List;

/**
 * By GT.
 */
public class SearchItem implements Serializable {
    public String Description;
    public List<Product> Products;
    
}
