package agoto.io.thewarehouse.models;

import java.io.Serializable;
import java.util.List;

/**
 * By GT.
 */
public class SearchResult implements Serializable{

    public Long HitCount;
    public List<SearchItem> Results;


    @Override
    public String toString() {
        return "SearchResult{" +
                "HitCount=" + HitCount +
                ", Results=" + Results +
                '}';
    }
}
