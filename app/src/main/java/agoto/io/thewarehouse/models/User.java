package agoto.io.thewarehouse.models;

import java.io.Serializable;

/**
 * By GT.
 */
public class User implements Serializable {

    public String ProdQAT;
    public String UserID;

    @Override
    public String toString() {
        return "User{" +
                "ProdQAT='" + ProdQAT + '\'' +
                ", UserID='" + UserID + '\'' +
                '}';
    }
}
