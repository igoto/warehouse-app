package agoto.io.thewarehouse.models;

import java.io.Serializable;

/**
 * By GT.
 */
public class Price implements Serializable {

    public String Price;
    public String Type;
    public String WasPrice;// – (Currency) The old price if we can claim discount, otherwise blank string
    public String Savings;// – (Currency) = WasPrice – Price
    public String Discount; // – (Currency) = Category discount %
    public String ValidUntil;

}
