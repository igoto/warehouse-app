# The Warehouse App Readme #

Version supported Android 4.1 and above.

### Useful Links ###
* [Source Code](https://bitbucket.org/igoto/warehouse-app/src) 
* [APK File](https://bitbucket.org/igoto/warehouse-app/downloads/warehouse-app.apk) 
* You can also find the downloadable apk `warehouse-app.apk` in the [downloads](https://bitbucket.org/igoto/warehouse-app/downloads) section of this repository.
* [Detail Documentation](https://bitbucket.org/igoto/warehouse-app/wiki/Home) 

### To run the app in emulator ###
* Please note barcode scanning feature might not work in emulator
* Run Android Emulator on your system.
* Download [warehouse-app.apk](https://bitbucket.org/igoto/warehouse-app/downloads/warehouse-app.apk) file. 
* Drag and drop the downloaded apk to the emulator window.

### To run the app in mobile device ###
* You might need to enable developer options on your mobile device. [Click here to know more](http://www.greenbot.com/article/2457986/how-to-enable-developer-options-on-your-android-phone-or-tablet.html)
* You need to allow apps from unknown sources 
    1. In your mobile go to `Settings -> Security`
    2. Just check `Unknown sources` and you're all set.
    3. [More Info](http://gs4.wonderhowto.com/forum/enable-unknown-sources-android-install-apps-outside-play-store-0150603/)
* Download [warehouse-app.apk](https://bitbucket.org/igoto/warehouse-app/downloads/warehouse-app.apk) file. 
* Install the apk file. 
* The app name is `The Warehouse` and icon is default Android icon

### To set up the code ###

* Clone the repository :  `git@bitbucket.org:igoto/warehouse-app.git`
* Open the project in Android studio. : `File > Open` menu
* Run the app using `run` button or `Run > Run app ` menu 
* Note : You might need to download or update to the latest Android studio and Android SDK/build tools.

### Test Cases ###
* Unit test cases : `app/src/test/java`
* Instrumented Test cases : `app/src/androidTest/java agoto.io.thewarehouse.instrumentedTests` 
* Integration Test cases : `src/androidTest/java agoto.io.thewarehouse.integrationTests`
* UI Automation Test cases : `src/androidTest/java agoto.io.thewarehouse.uiAutomationTests`


### Libraries Used ###

* [Android Support Libraries](https://developer.android.com/topic/libraries/support-library/features.html)
* [Retrofit](http://square.github.io/retrofit/) for making rest api calls. 
* [Rx Android](https://github.com/ReactiveX/RxAndroid) for asynchronous programming with observable streams.
* [Fresco](http://frescolib.org/) for displaying image.
* [ZXing Orient](https://github.com/SudarAbisheck/ZXing-Orient) for scanning barcodes.

#### UI Libraries ####
* [Floating Action Menu](https://github.com/futuresimple/android-floating-action-button)
* [Smooth Progress Bar](https://github.com/castorflex/SmoothProgressBar)
* [Material Search View](https://github.com/Mauker1/MaterialSearchView)

#### Testing Libraries  ####
* [Espresso](https://google.github.io/android-testing-support-library/docs/espresso/)
* [Mockwebserver](https://github.com/square/okhttp/tree/master/mockwebserver) for mocking http requests.

### Notes ###
* Was not sure about type of field in response so used Field Types as String and not as Integer or Long in lot of places for simplicity


### Developer ###

* Gaurav Tiwari